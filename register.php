<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php


function checkEmailAdress($email) {
  //check if the text in the field contains gmail.com
  if(preg_match("/\b(@gmail.com)\b/", $_POST['email'])) {
    return true;
  }
  else {
    return false;
  }
} 

function checkUserName($email) {
    //explode the email into username and domain
    $explodeEmail = explode('@',$email);
    if(strlen($explodeEmail[0]) < 7) {
      return true;
    }
    else {
      return false;
    }
}

// define the variables and set them to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = $dateobErr = "";
$name = $email = $gender = $dateob = $website = "";



if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // check if the field is empty - if it is - print the error
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);

    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }

    // check if name contains over 2 characters
    if(strlen($name) < 3) {
      $nameErr = "Length of name must be over 2 characters";
    }
  }

  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
    
    // check if the email contains more than 6 characters
    elseif(checkUserName($_POST["email"]) == true) {
      $emailErr = "Length of email must be over 6 characters";
    }
    
    elseif (checkEmailAdress($_POST["email"]) == true) {
        $emailErr = "";
    }
      else {
        $emailErr = "You MUST use a gmail address";
      }

    }
  }
  
  

  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);

    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["dateob"])) {
    $dateobErr = "Date of birth is required";
  } else {
    $dateob = test_input($_POST["dateob"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }

  

  if (!empty($nameErr) or !empty($emailErr) or !empty($websiteErr) or !empty($dateobErr) or !empty($genderErr)) {

    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&dateob=" . urlencode($_POST["dateob"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&dateobErr=" . urlencode($dateobErr);
    $params .= "&genderErr=" . urlencode($genderErr);

    header("Location: index.php?" . $params);
  }  else {
    echo "<h2>Your Input:</h2>";
    echo "Name: " . $_POST['name'];
    echo "<br>";
    echo "Email: " . $_POST['email'];
    echo "<br>";
    echo "Website: " . $_POST['website'];
    echo "<br>";

    // print the date of birth in a day-month-year-day_of_the_week format
    $new_date = date('d-m-Y-l', strtotime($_POST['dateob']));
    echo $new_date;
    echo "<br>";
    echo "Gender: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";
    echo "<a href=\"index.php\">Back to Form</a>";
  }


function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}



?>
</body>
</html>


<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
    $name = $email = $website = $dateob = $gender = "";
    $nameErr = $emailErr = $dateobErr = $websiteErr = $genderErr =  "";

    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['website'])) { $website = $_GET['website']; }
    if (isset($_GET['dateob'])) { $comment = $_GET['dateob']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['websiteErr'])) { $websiteErr = $_GET['websiteErr']; }
    if (isset($_GET['dateobErr'])) { $dateobErr = $_GET['dateobErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }
?>

<h2>PHP Form Validation - Fugly No CSS Edition</h2>
<p><span class="error">* required field.</span></p>
<form method="post" action="register.php">  
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Date of Birth: <input type="date" name="dateob"><?php echo $dateob;?>
  <span class="error">* <?php echo $dateobErr;?></span>
  <br><br>
  Gender:
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>
</body>
</html>

